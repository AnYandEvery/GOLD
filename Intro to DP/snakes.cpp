/*
http://www.usaco.org/index.php?page=viewproblem2&cpid=945
*/
#include <bits/stdc++.h>
using namespace std;
#define ll long long
int main(){
    freopen("snakes.in", "r", stdin);
    freopen("snakes.out", "w", stdout);
    int n, k; cin >> n >> k;
    int snakes[n];
    int dp[n][k+1];
    for(auto & i : dp)for(auto & j : i)j = 1000000000;
    int tot = 0;
    for(int i = 0; i < n; i++){
        cin >> snakes[i];
        tot += snakes[i];
    }
    int mx_ = snakes[0];
    for(int i = 0; i < n; i++){
        mx_ = max(mx_, snakes[i]);
        dp[i][0] = mx_*(i+1);
    }
    for(int i = 0; i < n; i++){
        int cur = snakes[i];
        for(int j = i; j < n; j++){
            cur = max(cur, snakes[j]);
            for(int z = 0; z < k; z++){
                int add = 0;
                if(i!=0)add = dp[i-1][z];
                dp[j][z+1] = min(dp[j][z+1], add + ((j-i+1)*cur));
            }
        }
    }
    cout << dp[n-1][k] - tot << endl;
}