/*
http://www.usaco.org/index.php?page=viewproblem2&cpid=993
*/
#include <bits/stdc++.h>
using namespace std;
#define ll long long
int main(){
    freopen("time.in", "r", stdin);
    freopen("time.out", "w", stdout);
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, m, c; cin >> n >> m >> c;
    ll dp[1010][n];
    memset(dp, -1, sizeof(dp));
    vector<vector<int>> adjList(n);
    int prizes[n];
    for(int i = 0; i < n; i++)cin >> prizes[i];
    for(int i = 0; i < m; i++){
        int a, b; cin >> a >> b;
        a--; b--;
        adjList[a].push_back(b);
    }
    dp[0][0] = 0;
    for(int i = 0; i < 1009; i++){
        for(int pl = 0; pl < n; pl++){
            if(dp[i][pl]==-1)continue;
            for(auto & e : adjList[pl]){
                dp[i+1][e] = max(dp[i+1][e], dp[i][pl] + prizes[e]);
            }
        }
    }
    ll mx = 0;
    for(int i = 0; i < 1010; i++){
        ll cur = dp[i][0] - c*i*i;
        mx = max(cur, mx);
    }
    cout << mx << endl;
}