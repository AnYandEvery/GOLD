/*
https://cses.fi/problemset/task/1745
*/
#include <bits/stdc++.h>
using namespace std;
int main(){
    int n; cin >> n;
    bool possible[100001];
    for(auto & i : possible) i = false;
    possible[0] = true;
    int tot = 0;
    int coins[n];
    for(int i = 0; i < n; i++)cin >> coins[i];
    for(int i = 0;i < n; i++){
        for(int x = 100000; x >= 0; x--){
            if(!possible[x])continue;
            if(possible[x+coins[i]])continue;
            possible[x+coins[i]] = true;
            tot += 1;
        }
    }
    cout << tot << endl;
    for(int i = 1; i < 100001; i++){
        if(possible[i])cout << i << " ";
    }
    cout << endl;
}