/*
https://cses.fi/problemset/task/1093
*/
#include <bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
int main(){
    int n; cin >> n;
    int sum = (1+n)*(n)*0.5;
    int dp[sum/2 + 1];
    for(auto & i : dp)i = 0;
    dp[0] = 1;
    for(int i = 1; i < n; i++){
        for(int j = sum/2; j>=0; j--){
            if(dp[j]==0)continue;
            if((j+i)>(sum/2))continue;
            dp[i+j] += dp[j];
            dp[i+j]%=mod;
        }
    }
    if(sum%2==1)cout << 0 << endl;
    else cout << dp[sum/2] << endl;
}