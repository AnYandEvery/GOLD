/*
https://cses.fi/problemset/task/1635
*/
#include <bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
#define ll long long
int main(){
    int n, x; cin >> n >> x;
    int coins[n];
    for(int i = 0; i < n; i++)cin >> coins[i];
    ll dp[x+1];
    for(auto & i : dp)i = 0;
    dp[0] = 1;
    for(int i = 0; i < x; i++){
        for(int cX = 0; cX < n; cX++){
            if(dp[i]==0)continue;
            if((i+coins[cX]) > x)continue;
            dp[i+coins[cX]] += dp[i];
            dp[i+coins[cX]]%=mod;
        }
    }
    cout << dp[x] << endl;
}