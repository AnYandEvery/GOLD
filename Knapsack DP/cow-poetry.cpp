/*
http://www.usaco.org/index.php?page=viewproblem2&cpid=897
*/
//reason for being stuck: 1-5000 requires the list to be 5001 to accomodate 5000
#include <bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
#define ll long long
ll binExp(ll x, ll p){
    x %= mod;
    if(p==0)return 1;
    ll ans = binExp(x, p/2);
    ans = (ans*ans)%mod;
    if(p%2==1)ans *= x;
    ans %= mod;
    return ans;
}
int main(){
    freopen("poetry.in", "r", stdin);
    freopen("poetry.out", "w", stdout);
    ll n, m, k; cin >> n >> m >> k;
    pair<ll, ll> words[n]; 
    int rhymePattern[26];
    for(auto & i : rhymePattern)i = 0;
    for(ll i = 0; i < n; i++){
        ll a, b; cin >> a >> b;
        words[i] = {a, b}; //syllables, rhyme class
    }
    ll dp[k];
    ll differentEnding[5005];
    for(auto & i : dp)i = 0;
    for(auto & i : differentEnding)i = 0;
    for(ll i = 0; i < m; i++){
        char a; cin >> a;
        rhymePattern[a-'A']+=1;
    }
    dp[0] = 1;
    for(ll i = 0; i <= k; i++){
        if(dp[i]==0)continue;
        for(ll j = 0; j < n; j++){
            ll cur = words[j].first + i;
            if(cur > k)continue;
            if(cur == k){
                differentEnding[words[j].second] = (differentEnding[words[j].second] + dp[i]+mod)%mod;
            }
            if(cur < k){
                dp[cur] = (dp[cur]+dp[i]+mod)%mod;
            }
        }
    }
    ll ret = 1;
    for(ll p : rhymePattern){
        if(p==0)continue;
        ll cur = 0;
        for(ll z : differentEnding){
            if(z==0)continue;
            cur = (cur+binExp(z, p)+mod)%mod;
        }
        ret *= cur;
        ret %= mod;
    }
    cout << ret << endl;
}