/*
NOI: https://oj.uz/problem/view/NOI18_knapsack
*/
#include <bits/stdc++.h>
using namespace std;
#define ll long long
//idea 1: Ki beyond 2000 is the same as Ki of 2000
struct product{
    ll worth; ll weight; ll num;
    void init(ll w, ll w2, ll n){
        worth = w;
        weight = w2;
        num = n;
    }
};
//n<=1e5, ki <= 1e9, s <= 2000
int main(){
    ll s, n; cin >> s >> n;
    product products[n];
    for(ll i = 0; i < n; i++){
        ll a, b, c; cin >> a >> b >> c;//worth, weight, numCopies 
        if(c>2010)c=2010;
        product temp; temp.init(a, b, c);
        products[i] = temp;
    }
    ll dp[s+1]; for(auto & i : dp)i = 0;
    for(int i = 0; i < n; i++){
        ll numUse[s+1]; for(auto & zaa : numUse)zaa = 0;
        for(ll x = 0; x < s; x++){
            ll curW = products[i].weight + x;
            if(curW > s)continue;
            if(dp[x]==0&&x!=0)continue;
            if(numUse[x] == products[i].num)continue;
            ll cur = dp[x] + products[i].worth;
            ll prev = dp[curW];
            if(cur > prev){
                numUse[curW] = numUse[x] + 1;
                dp[curW] = cur;
            }
            if(cur == prev){
                numUse[curW] = min(numUse[x]+1, numUse[curW]);
            }
        }
    }
    ll mx = 0;
    for(auto & i : dp){
        if(i > mx)mx = i;
    }
    cout << mx << endl;
}