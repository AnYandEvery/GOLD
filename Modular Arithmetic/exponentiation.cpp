/*
https://cses.fi/problemset/task/1095
*/
#include <bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
#define ll long long
ll binPow(ll x, ll p){
    if(p==0)return 1;
    if(p%2==0){
        return binPow(x*x%mod, p/2);
    }
    return binPow(x*x%mod, p/2)*x%mod;
}
int main(){
    int n; cin >> n;
    while(n--){
        ll a, b; cin >> a >> b;
        cout << binPow(a, b)%mod << endl;
    }
}