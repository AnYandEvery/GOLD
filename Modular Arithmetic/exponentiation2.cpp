/*
https://cses.fi/problemset/task/1712
*/
#include <bits/stdc++.h>
using namespace std;
#define ll long long
ll binExp(ll x, ll p, int mod){
    if(p==0)return 1;
    if(p%2==1){
        return binExp(x*x%mod, p/2, mod)*x%mod;
    }
    return binExp(x*x%mod, p/2, mod)%mod;
}
int main(){
    int n; cin >> n;
    while(n--){
        ll a, b, c; cin >> a >> b >> c;
        ll p = binExp(b, c, 1e9+6);
        ll ret = binExp(a, p, 1e9+7);
        cout << ret << endl;
    }
}