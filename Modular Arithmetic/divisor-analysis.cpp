#include <bits/stdc++.h>
using namespace std;
#define ll long long
const int mod = 1e9+7;
vector<pair<int, int>> primeFact;
ll binExp(ll b, ll p, int mod){
    if(p==0)return 1;
    if(p%2){
        return binExp(b*b%mod, p/2, mod)*b%mod;
    }
    return binExp(b*b%mod, p/2, mod)%mod;
}
ll sumGeo(ll a, ll r, ll n){
    ll numerator = a%mod*(binExp(r, n, mod) - 1 + mod)%mod;
    ll denom = r - 1; denom = binExp(denom, mod-2, mod);
    return numerator*denom%mod;
}
int sum;
ll numDivisors(){
    ll ret = 1;
    for(auto c : primeFact){
        ret *= (c.second+1);
        ret %= mod;
    }
    return ret;
}
ll sumDivisor(){
    ll ret = 1;
    for(auto c : primeFact){
        ll temp = sumGeo(1, c.first, c.second+1);
        ret *= temp;
        ret %= mod;
    }
    return ret;
}
ll findSquare(ll a){
    for(int i = 0; i < a; i++){
        if(binExp(i, 2, mod) == a)return i;
    }
    return -1;
}
ll prodDivisor(){
    ll ret = 1;
    for(auto c : primeFact){
        ll temp = binExp(c.first, c.second, mod);
        ret *= temp;
        ret %= mod;
    }
    if(sum%2==1){
        return ;
    }
}
int main(){
    int n; cin >> n;
    primeFact.resize(n);
    for(int i = 0; i < n; i++){
        cin >> primeFact[i].first >> primeFact[i].second;
    }

    cout << numDivisors() << endl;
    sum = (double)(numDivisors()%mod);
    cout << sumDivisor() << endl;
    cout << prodDivisor() << endl;
}