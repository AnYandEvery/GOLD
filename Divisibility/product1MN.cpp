/*
https://codeforces.com/problemset/problem/1514/C
*/
#include <bits/stdc++.h>
using namespace std;
#define ll long long
int main(){
    int n; cin >> n;
    ll m = 1;
    vector<int> ret;
    for(int i = 1; i < n; i++){
        if(__gcd(i, n)!=1)continue;
        m*=i;
        m%=n;
        ret.push_back(i);
    }
    if(m!=1)cout << ret.size()-1 << endl;
    else cout << ret.size() << endl;
    for(int i : ret){
        if(i==m && m != 1)continue;
        cout << i << " ";
    }

    cout << endl;
}
