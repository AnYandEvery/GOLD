#include <bits/stdc++.h>
const int MAXN = 1e5;
/*
https://cses.fi/problemset/task/1713
*/
/*
O(sqrt(x)) solution
#include <bits/stdc++.h>
using namespace std;
int main(){
    int n; cin >> n;
    for(int i = 0; i < n; i++){
        int a; cin >> a;
        int tot = 0;
        for(int j = 1; j*j <= a; j++){
            if(a%j!=0)continue;
            if(a%j==0)tot+=2;
            if(j*j==a)tot-=1;
        }
        cout << tot << endl;
    }
}
*/
/*
O((x+n)log x) find prime components of x
Sieve of Eratosthenes
*/
std::vector <int> prime;
bool is_composite[MAXN];

void sieve (int n) {
	std::fill (is_composite, is_composite + n, false);
	for (int i = 2; i < n; ++i) {
		if (!is_composite[i]) prime.push_back (i);
		for (int j = 2; i * j < n; ++j)
			is_composite[i * j] = true;
	}
}

/*
linear sieve: each number is represented by smallest prime to make
each composite only represented once
time complexity (O(n))
*/
std::vector <int> prime;
bool is_composite[MAXN];

void linearSieve (int n) {
	std::fill (is_composite, is_composite + n, false);
	for (int i = 2; i < n; ++i) {
		if (!is_composite[i]) prime.push_back (i);
		for (int j = 0; j < prime.size () && i * prime[j] < n; ++j) {
			is_composite[i * prime[j]] = true;
			if (i % prime[j] == 0) break;
		}
	}
}
