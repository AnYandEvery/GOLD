#include <bits/stdc++.h>
using namespace std;
#define ll long long
int main(){
    int t; cin >> t;
    while(t--){
        int n; cin >> n;
        string s; cin >> s;
        map<pair<int, int>, int> mp;
        ll curD = 0, curK = 0;
        int ret = 0;
        for(int i = 0; i < n; i++){
            if(s[i]=='D')curD++;
            else curK++;
            pair<int, int> temp = {curD, curK};
            int g = __gcd(curD, curK);
            temp.first/=g; temp.second/=g;
            if(mp.find(temp)==mp.end()){
                mp[temp] = 0;
            }
            mp[temp]++;
            cout << mp[temp] << " ";
        }
        cout << endl;
    }
}