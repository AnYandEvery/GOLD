/*
Euclidean algorithm
gcd(a, b) = a if b = 0, gcd(b, a mod b) if b !=0
*/
int gcd(int a, int b){
    if(b==0)return a;
    return gcd(b, a%b);
}
/*
lcm = a*b/gcd(a, b)
*/
int lcm(int a, int b){
    return (a/gcd(a, b))*b;
}