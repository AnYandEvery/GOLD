#include <bits/stdc++.h>
using namespace std;
#define ll long long
ll pow(ll x, int p){
    if(p==0)return 1;
    if(p%2==0){
        return pow(x*x, p/2);
    }
    if(p%2==1){
        return pow(x*x, p/2)*x;
    }
    return -1;
}
vector<int> val;
int binarySearch(int find){
    int li = 0, hi = val.size()-1;
    while(li <= hi){
        int mi = (li+hi+1)/2;
        if(val[mi] > find)hi = mi-1;
        else if(val[mi] < find)li = mi+1;
        else return mi;
    }
    return -1;
}
int main(){
    int n, k; cin >> n >> k;
    vector<int> generatePower;
    int temp = 1;ll cur = pow(temp, k);
    while(cur <= 100000){
        generatePower.push_back(cur);
        temp++; cur = pow(temp, k);
    }
    val.resize(n);
    for(int i = 0; i < n; i++)cin >> val[i];
    sort(val.begin(), val.end());
    int ret = 0;
    for(int i = 0; i < n-1; i++){
        for(int c : generatePower){
            if(c%val[i]!=0)continue;
            int pivot = binarySearch(c/val[i]);
            if(pivot > i)ret++;
        }
    }
    cout << ret << endl;
}