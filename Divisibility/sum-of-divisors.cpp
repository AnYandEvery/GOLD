#include <bits/stdc++.h>
using namespace std;
#define ll long long
const ll mod = 1e9+7;
int main(){
    ll n; cin >> n;
    ll tot = 0; ll iR = 1;
    while(iR <= n){
        ll i = iR%mod;
        ll q = n/iR; ll newI = n/q + 1; ll newIReplace = newI; 
        q %= mod; newI %= mod;
        ll s1 = (i+newI-1)%mod;
        ll s2 = (newI-i+mod)*s1%mod;
        ll cur = s2;
        if(cur%2==1){
            if(cur >= mod)cur -= mod;
            else cur += mod;
        }
        if(i < 0 || iR < 0 || newI < 0 || s1 < 0 || s2 < 0 || cur < 0){
            cout <<"RED FLAG";
        }
        cur/=2; cur%=mod;
        ll temp = q%mod*cur%mod;
        tot += temp;
        tot%=mod;
        iR = newIReplace;
    }
    cout << tot << endl;
}