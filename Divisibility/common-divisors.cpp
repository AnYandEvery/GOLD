#include <bits/stdc++.h>
using namespace std;
bool factors[1000001];
int ret = 1;
void setN(int a){
    for(int i = 1; i*i <= a; i++){
        if(a%i!=0)continue;
        if(factors[i])ret = max(ret, i);
        factors[i] = true;
        if(i*i==a)continue;
        if(factors[a/i])ret = max(ret, a/i);
        factors[a/i] = true;
    }
}
int main(){
    for(auto & i : factors)i = false;
    int n; cin >> n;
    for(int i = 0; i < n; i++){
        int a; cin >> a;
        setN(a);
    }
    cout << ret << endl;

}